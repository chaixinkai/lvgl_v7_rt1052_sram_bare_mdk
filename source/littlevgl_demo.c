/*
 * Copyright 2019 NXP
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "fsl_debug_console.h"
#include "littlevgl_support.h"
#include "board.h"
#include "lvgl.h"
#include "pin_mux.h"
/*******************************************************************************
 * Definitions
 ******************************************************************************/

/*******************************************************************************
 * Prototypes
 ******************************************************************************/
extern void lv_demo_widgets(void);
static void AppTask(void)
{
        lv_init();
        lv_port_disp_init();
        lv_port_indev_init();
        
        lv_demo_widgets();
//        lv_obj_t * arc = lv_arc_create(lv_scr_act(), NULL);
//        lv_arc_set_end_angle(arc, 200);
//        lv_obj_set_size(arc, 150, 150);
//        lv_obj_align(arc, NULL, LV_ALIGN_CENTER, 0, 0);
        for (;;)
        {
                lv_tick_inc(5);
                lv_task_handler();
        }
}

/*******************************************************************************
 * Code
 ******************************************************************************/
/*!
 * @brief Main function
 */
int main(void)
{
    /* Init board hardware. */
    BOARD_ConfigMPU();
    BOARD_InitPins();
    BOARD_InitI2C1Pins();
    BOARD_InitSemcPins();
    BOARD_BootClockRUN();
    BOARD_InitDebugConsole();
    AppTask();


    for (;;)
    {
    } /* should never get here */
}

